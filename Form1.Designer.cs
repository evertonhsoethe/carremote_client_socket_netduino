﻿namespace TesteRobuino
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btDesconectar = new System.Windows.Forms.Button();
            this.btEsquerda = new System.Windows.Forms.Button();
            this.btDireita = new System.Windows.Forms.Button();
            this.btRe = new System.Windows.Forms.Button();
            this.btAcelerar = new System.Windows.Forms.Button();
            this.cbStatus = new System.Windows.Forms.CheckBox();
            this.btConectar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btDesconectar
            // 
            this.btDesconectar.Enabled = false;
            this.btDesconectar.Location = new System.Drawing.Point(83, 2);
            this.btDesconectar.Name = "btDesconectar";
            this.btDesconectar.Size = new System.Drawing.Size(92, 23);
            this.btDesconectar.TabIndex = 1;
            this.btDesconectar.Text = "Desconectar";
            this.btDesconectar.UseVisualStyleBackColor = true;
            this.btDesconectar.Click += new System.EventHandler(this.btDesconectar_Click);
            // 
            // btEsquerda
            // 
            this.btEsquerda.Image = global::TCPSocketClient.Properties.Resources._1leftarrow;
            this.btEsquerda.Location = new System.Drawing.Point(19, 141);
            this.btEsquerda.Name = "btEsquerda";
            this.btEsquerda.Size = new System.Drawing.Size(75, 23);
            this.btEsquerda.TabIndex = 2;
            this.btEsquerda.Text = "&A";
            this.btEsquerda.UseVisualStyleBackColor = true;
            this.btEsquerda.Click += new System.EventHandler(this.btEsquerda_Click);
            // 
            // btDireita
            // 
            this.btDireita.Image = global::TCPSocketClient.Properties.Resources._1rightarrow;
            this.btDireita.Location = new System.Drawing.Point(181, 141);
            this.btDireita.Name = "btDireita";
            this.btDireita.Size = new System.Drawing.Size(75, 23);
            this.btDireita.TabIndex = 3;
            this.btDireita.Text = "&D";
            this.btDireita.UseVisualStyleBackColor = true;
            this.btDireita.Click += new System.EventHandler(this.btDireita_Click);
            // 
            // btRe
            // 
            this.btRe.Image = global::TCPSocketClient.Properties.Resources._1downarrow;
            this.btRe.Location = new System.Drawing.Point(100, 141);
            this.btRe.Name = "btRe";
            this.btRe.Size = new System.Drawing.Size(75, 23);
            this.btRe.TabIndex = 5;
            this.btRe.Text = "&S";
            this.btRe.UseVisualStyleBackColor = true;
            this.btRe.Click += new System.EventHandler(this.btRe_Click);
            // 
            // btAcelerar
            // 
            this.btAcelerar.BackColor = System.Drawing.SystemColors.Control;
            this.btAcelerar.Image = global::TCPSocketClient.Properties.Resources._1uparrow1;
            this.btAcelerar.Location = new System.Drawing.Point(100, 112);
            this.btAcelerar.Name = "btAcelerar";
            this.btAcelerar.Size = new System.Drawing.Size(75, 23);
            this.btAcelerar.TabIndex = 4;
            this.btAcelerar.Text = "&W";
            this.btAcelerar.UseVisualStyleBackColor = false;
            this.btAcelerar.Click += new System.EventHandler(this.btAcelerar_Click);            
            // 
            // cbStatus
            // 
            this.cbStatus.AutoSize = true;
            this.cbStatus.Enabled = false;
            this.cbStatus.Location = new System.Drawing.Point(181, 6);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(56, 17);
            this.cbStatus.TabIndex = 6;
            this.cbStatus.Text = "Status";
            this.cbStatus.UseVisualStyleBackColor = true;
            // 
            // btConectar
            // 
            this.btConectar.Location = new System.Drawing.Point(1, 2);
            this.btConectar.Margin = new System.Windows.Forms.Padding(2);
            this.btConectar.Name = "btConectar";
            this.btConectar.Size = new System.Drawing.Size(77, 23);
            this.btConectar.TabIndex = 7;
            this.btConectar.Text = "Conectar";
            this.btConectar.UseVisualStyleBackColor = true;
            this.btConectar.Click += new System.EventHandler(this.btConectar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btConectar);
            this.Controls.Add(this.cbStatus);
            this.Controls.Add(this.btRe);
            this.Controls.Add(this.btAcelerar);
            this.Controls.Add(this.btDireita);
            this.Controls.Add(this.btEsquerda);
            this.Controls.Add(this.btDesconectar);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private System.Windows.Forms.Button btDesconectar;
        private System.Windows.Forms.Button btEsquerda;
        private System.Windows.Forms.Button btDireita;
        private System.Windows.Forms.Button btRe;
        private System.Windows.Forms.CheckBox cbStatus;
        private System.Windows.Forms.Button btAcelerar;
        private System.Windows.Forms.Button btConectar;
    }
}

