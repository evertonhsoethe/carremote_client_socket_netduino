﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteRobuino
{
    public partial class Form1 : Form
    {
        Socket Conexao = new Socket();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.D)
            {
                btDireita.BackColor = SystemColors.ActiveCaption;
                //Conexao.VirarDireita();
            }
            if (e.KeyCode == Keys.A)
            {
                btEsquerda.BackColor = SystemColors.ActiveCaption;
                //Conexao.VirarEsquerda();
            }
            if (e.KeyCode == Keys.W)
            {
                btAcelerar.BackColor = SystemColors.ActiveCaption;
                //Conexao.Acelerar();
            }
            if (e.KeyCode == Keys.S)
            {
                btRe.BackColor = SystemColors.ActiveCaption;
                //Conexao.Re();
            }

        }

        private void btConectar_Click(object sender, EventArgs e)
        {            
            if (Conexao.Conecta())
            {
                btConectar.Enabled = false;
                cbStatus.Checked = true;
                btDesconectar.Enabled = true;
            }
        }

        private void btAcelerar_Click(object sender, EventArgs e)
        {
            if (Conexao.Conectado)
                Conexao.Acelerar();
        }

        private void btRe_Click(object sender, EventArgs e)
        {
            if (Conexao.Conectado)
                Conexao.Re();
        }

        private void btEsquerda_Click(object sender, EventArgs e)
        {
            if (Conexao.Conectado)
                Conexao.VirarEsquerda();
        }

        private void btDireita_Click(object sender, EventArgs e)
        {
            if (Conexao.Conectado)
                Conexao.VirarDireita();
        }

        private void btDesconectar_Click(object sender, EventArgs e)
        {
            if (Conexao.Conectado)
                Conexao.FechaConexao();
            btConectar.Enabled = true;
            btDesconectar.Enabled = false;
            cbStatus.Checked = false;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.D)
            {
                btDireita.BackColor = SystemColors.Control;
                Conexao.NVirarDireita();
            }
            if (e.KeyCode == Keys.A)
            {
                btEsquerda.BackColor = SystemColors.Control;
                Conexao.NVirarEsquerda();
            }
            if (e.KeyCode == Keys.W)
            {
                btAcelerar.BackColor = SystemColors.Control;
                Conexao.NAcelerar();
            }
            if (e.KeyCode == Keys.S)
            {
                btRe.BackColor = SystemColors.Control;
                Conexao.NRe();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Conexao.Conectado)
            {
                Conexao.FechaConexao();                
            }
        }
    }
}


