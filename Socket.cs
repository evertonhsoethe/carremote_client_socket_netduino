﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace TesteRobuino
{
    public class Socket
    {
        private StreamReader clientStreamReader;
        private StreamWriter clientStreamWriter;
        public bool Conectado = false;

        public bool Conecta()
        {
            try
            {               
                IPAddress localAddr = IPAddress.Parse("192.168.169.101");
                TcpClient tcpClient = new TcpClient("192.168.169.101", 10100);
                Console.WriteLine("Tentando conectar ao Robuino...");

                NetworkStream clientSockStream = tcpClient.GetStream();
                clientStreamReader = new StreamReader(clientSockStream);
                clientStreamWriter = new StreamWriter(clientSockStream);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Conectado = false;
                return false;
            }
            Conectado = true;
            return true;            
        }
        /////////////////////////////////////////////////////////////////////////////
        ///Eventos
        /////////////////////////////////////////////////////////////////////////////
        public void Acelerar()
        {
            try
            {
                //Acelerador
                clientStreamWriter.WriteLine("W");
                clientStreamWriter.Flush();            
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void NAcelerar()
        {
            try
            {
                //NAcelerador
                clientStreamWriter.WriteLine("X");
                clientStreamWriter.Flush();        
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void Re()
        {
            try
            {
                //Re
                clientStreamWriter.WriteLine("S");
                clientStreamWriter.Flush();
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void NRe()
        {
            try
            {
                //NRe
                clientStreamWriter.WriteLine("T");
                clientStreamWriter.Flush();
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void VirarDireita()
        {
            try
            {
                //Direita
                clientStreamWriter.WriteLine("D");
                clientStreamWriter.Flush();
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void NVirarDireita()
        {
            try
            {
                //NVirarDireita
                clientStreamWriter.WriteLine("E");
                clientStreamWriter.Flush();
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void VirarEsquerda()
        {
            try
            {
                //VirarEsquerda
                clientStreamWriter.WriteLine("A");
                clientStreamWriter.Flush();
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void NVirarEsquerda()
        {
            try
            {
                //NVirarEsquerda
                clientStreamWriter.WriteLine("B");
                clientStreamWriter.Flush();
            }
            catch (Exception se)
            {
                Console.WriteLine(se.StackTrace);
            }
        }
        public void FechaConexao()
        {
            clientStreamWriter.WriteLine("L");
            clientStreamWriter.Flush();
            clientStreamReader.Close();
            clientStreamWriter.Close();
            Conectado = false;
        }
    }
}

